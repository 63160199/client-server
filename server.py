import socket

def main():
    host = 'localhost'
    port = 12345
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    print(f"Server listening on {host}:{port}")
    while True:
        conn, addr = s.accept()
        print(f"Connected to client: {addr}")
        try:
            num = 1
            while num <= 100:
                conn.send(str(num).encode())
                data = conn.recv(1024)
                num = int(data.decode())
                print(f"Received from client and incremented by 1: {num}")
                num += 1
        except (ValueError, ConnectionResetError):
            print("Invalid data received or client disconnected.")
        finally:
            conn.close()
            print("Connection closed with client.")
            break 
if __name__ == "__main__":
    main()
